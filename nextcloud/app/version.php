<?php 
$OC_Version = array(27,1,2,1);
$OC_VersionString = '27.1.2';
$OC_Edition = '';
$OC_Channel = 'stable';
$OC_VersionCanBeUpgradedFrom = array (
  'nextcloud' => 
  array (
    '26.0' => true,
    '27.0' => true,
    '27.1' => true,
  ),
  'owncloud' => 
  array (
    '10.11' => true,
  ),
);
$OC_Build = '2023-10-05T10:53:45+00:00 11b2762d3cb72af264d03dcb201eabf0463dff0b';
$vendor = 'nextcloud';
